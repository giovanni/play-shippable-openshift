package controllers

import business.coffees.{Coffee, CoffeeDAO}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc.{Action, Controller}

object Application extends Controller {

  implicit val coffeesReads = Json.reads[Coffee]
  implicit val coffeesWrites: Writes[Coffee] = (
      (JsPath \ "name").write[String] and
      (JsPath \ "supID").write[Int] and
      (JsPath \ "price").write[Double] and
      (JsPath \ "sales").write[Int] and
      (JsPath \ "total").write[Int]
    )(unlift(Coffee.unapply))

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def databaseCall = Action {
    val coffeesDAO = new CoffeeDAO
    val query = coffeesDAO.listAllCoffesWithSuppliers
    Ok(views.html.database(query))
  }

  def searchCoffeBySupplierId(supID: Int) = Action {
    val coffeesDAO = new CoffeeDAO
    val query = coffeesDAO.searchCoffeBySupplierId(supID)
    Ok(Json.toJson(query))
  }

}