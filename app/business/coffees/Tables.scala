package business.coffees
import play.api.db.slick.Config.driver.simple._

/**
 * @author Giovanni Silva.
 *         11/27/14
 */
object Tables {
  val coffess = TableQuery[Coffees]
  val suppliers = TableQuery[Suppliers]
}
class Coffees(tag: Tag) extends Table[Coffee](tag, "COFFEES") {
  import business.coffees.Tables._
  def name = column[String]("COF_NAME", O.PrimaryKey)
  def supID = column[Int]("SUP_ID")
  def price = column[Double]("PRICE")
  def sales = column[Int]("SALES", O.Default(0))
  def total = column[Int]("TOTAL", O.Default(0))
  def * = (name, supID, price, sales, total) <> (Coffee.tupled, Coffee.unapply)
  // A reified foreign key relation that can be navigated to create a join
  def supplier = foreignKey("SUP_FK", supID, suppliers)(_.id)
}
class Suppliers(tag: Tag) extends Table[Supplier](tag, "SUPPLIERS") {
  def id = column[Int]("SUP_ID", O.PrimaryKey) // This is the primary key column
  def name = column[String]("SUP_NAME")
  def street = column[String]("STREET")
  def city = column[String]("CITY")
  def state = column[String]("STATE")
  def zip = column[String]("ZIP")
  // Every table needs a * projection with the same type as the table's type parameter
  def * = (id, name, street, city, state, zip) <> (Supplier.tupled, Supplier.unapply)
}
case class Coffee(name:String, supID: Int, price: Double, sales: Int, total: Int)
case class Supplier(id: Int, name: String, street: String, city: String, state: String, zip: String)
