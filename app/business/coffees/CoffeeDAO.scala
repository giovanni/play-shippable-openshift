package business.coffees

import play.api.db.slick.DB
import play.api.db.slick.Config.driver.simple._

import Tables._
import play.api.Play.current
/**
 * @author Giovanni Silva.
 *         11/27/14
 */
class CoffeeDAO {

  def listAllCoffesWithSuppliers: List[(String,String)] = {
    DB withSession { implicit session =>
      val q1 = for {
        (c,s) <- coffess innerJoin suppliers
      } yield (c.name, s.name)

      q1.list
    }

  }
  def searchCoffeBySupplierId(supplierId: Int): List[Coffee] = {
    DB withSession {implicit session =>
      coffess.filter(_.supID === supplierId).list
    }
  }
}
