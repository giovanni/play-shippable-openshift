Play application - Continuous Deployment
========================================

[![Build Status](https://api.shippable.com/projects/5474eaaed46935d5fbbe94ed/badge?branchName=master)](https://app.shippable.com/projects/5474eaaed46935d5fbbe94ed/builds/latest)

This project is able to be continuous delivery with [Shippable] and [Openshift].

The goal is push commits to server and be able to see this changes in openshift if the tests pass.
You could adapt this application to any environment, we will use the follow:

* Play with Scala and SBT. We will run tests and package the application
* Dart Web Project. We will compile to javascript for production, and run tests in the browser.
  You could use this as a example to compile any resources
* Postgresql Database. The database configuration will be selected automatic given the environment we are.
  In the developer machine it will use a database configured by hand, the continous integration and openshift will
  use another database as well

[Shippable]:http://www.shippable.com
[Openshift]:http://www.openshift.com
