# --- !Ups

insert into "SUPPLIERS" values (101, 'Acme, Inc.','99 Market Street','Groundsville', 'CA', '95199');
insert into "SUPPLIERS" values (49, 'Superior Coffee', '1 Party Place','Mendocino',    'CA', '95460');
insert into "SUPPLIERS" values (150, 'The High Ground', '100 Coffee Lane', 'Meadows','CA', '93966');

insert into "COFFEES" values ('Colombian', 101, 7.99, 0, 0);
insert into "COFFEES" values ('French_Roast', 49, 8.99, 0, 0);
insert into "COFFEES" values ('Espresso', 150, 9.99, 0, 0);
insert into "COFFEES" values ('Colombian_Decaf', 101, 8.99, 0, 0);
insert into "COFFEES" values ('French_Roast_Decaf', 49, 9.99, 0, 0);