module.exports = function(config) {
  config.set({
    basePath: '.',
    reporters: ['progress', 'junit'],
    frameworks: ['dart-unittest'],

    // list of files / patterns to load in the browser
    // all tests must be 'included', but all other libraries must be 'served' and
    // optionally 'watched' only.
    files: [
      'test/*_spec.dart',
      {pattern: '**/*.dart', watched: true, included: false, served: true},
      'packages/browser/dart.js'
    ],

    autoWatch: true,

    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 5000,

    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 120000,
    // Time for dart2js to run on Travis... [ms]
    browserNoActivityTimeout: 1500000,

    plugins: [
      'karma-dart',
      'karma-chrome-launcher',
      'karma-script-launcher',
      'karma-junit-reporter'
    ],
    customLaunchers: {
      'SL_Chrome': {
          base: 'SauceLabs',
          browserName: 'chrome',
          version: '35'
      },
      'SL_Firefox': {
          base: 'SauceLabs',
          browserName: 'firefox',
          version: '30'
      },
      DartiumWithWebPlatform: {
        base: 'Dartium',
        flags: ['--enable-experimental-web-platform-features'] }
    },

    browsers: ['DartiumWithWebPlatform'],

    junitReporter: {
      outputFile: 'shippable/testresults/karma.xml',
      suite: 'unit'
    },
  });
};