import business.coffees.CoffeeDAO
import org.specs2.mutable._
import play.api.test.WithApplication

/**
 * @author Giovanni Silva.
 *         11/27/14
 */
class CoffeeDAOSpec extends Specification {

  "An CoffeDAO" should  {
    val coffeeDAO = new CoffeeDAO
    "list all coffes and suppliers" in new WithApplication() {

      val result = coffeeDAO.listAllCoffesWithSuppliers
      result.size must be_>(1)
    }
    "filter coffees by supplierID" in new WithApplication() {
      val result = coffeeDAO.searchCoffeBySupplierId(101)
      result must have size (2)
      result.foreach(c => c.supID mustEqual 101 )
    }
  }

}
