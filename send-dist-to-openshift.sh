#!/bin/sh
sbt dist
cd target/universal/
unzip -o play-shippable-openshift-1.0-SNAPSHOT.zip
ssh $OPENSHIFT_REPO_URL 'mkdir -p ~/app-root/data/target'
rsync play-shippable-openshift-1.0-SNAPSHOT/* -rv $OPENSHIFT_REPO_URL:~/app-root/data/target/