name := """play-shippable-openshift"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.4"

javaOptions in Test += "-Dconfig.file=conf/test.conf"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws
)

libraryDependencies ++= Seq(
  "org.webjars" %% "webjars-play" % "2.3.0-2",
  "org.webjars" % "foundation" % "5.4.7",
  "net.sourceforge.htmlunit" % "htmlunit" % "2.15",
  "org.postgresql" % "postgresql" % "9.3-1100-jdbc41",
  "com.typesafe.play" %% "play-slick" % "0.8.0"
)
