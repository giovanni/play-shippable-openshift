#!/bin/bash

set -e

DART_DIST=dartsdk-linux-x64-release.zip
DARTIUM_DIST=dartium-linux-x64-release.zip

export DART_SDK="$HOME/dart-sdk"
export PATH="$DART_SDK/bin:$PATH"
export DARTIUM_BIN="$HOME/dartium/chrome"

if [ ! -d $DART_SDK ]; then
 echo "Fetching DART SDK"
 curl http://storage.googleapis.com/dart-archive/channels/stable/release/latest/sdk/$DART_DIST > $DART_DIST
 unzip $DART_DIST > /dev/null
 rm $DART_DIST
 mv dart-sdk $HOME
fi

if [ ! -d $HOME/dartium ]; then
 echo "Fetching DARTIUM BROWSER"
 curl http://storage.googleapis.com/dart-archive/channels/stable/raw/latest/dartium/$DARTIUM_DIST > $DARTIUM_DIST
 unzip $DARTIUM_DIST > /dev/null
 rm $DARTIUM_DIST
 mv dartium-* dartium
 mv dartium $HOME
fi
cd public
echo "Pub Install"
pub install
echo "Node Install"
npm install
echo "Dart 2 JS"
dart2js application.dart -o application.dart.js

sh -e /etc/init.d/xvfb start
chmod +x $DARTIUM_BIN
./node_modules/karma/bin/karma start --single-run --browsers Dartium --log-level debug